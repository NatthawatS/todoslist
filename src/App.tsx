import React, { FC } from 'react';
import { Provider } from 'react-redux';
import './App.css';
import Routes from './config/routes';
import store from './reducers';

const App: FC = () => (
  <Provider store={store}>
    <Routes />
  </Provider>
);

export default App;
