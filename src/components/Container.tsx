import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex: 1;
  height: 100vh;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background: -webkit-linear-gradient(-45deg, #9fb8ad 0%, #2bb5e8 100%);
`;
