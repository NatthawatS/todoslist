import React, { FC, useState } from 'react';
import { useHistory } from 'react-router-dom';
import uuid from 'uuid';
import {
  CheckOutlined,
  PlusOutlined,
  PoweroffOutlined,
  DeleteOutlined,
  EditOutlined,
} from '@ant-design/icons';
import { Drawer, Button, Form, Row, Input, Col } from 'antd';
import {
  CheckBoxStyled,
  CircleButtonStyled,
  Container,
  ListStyled,
  HeaderStyled,
  BodyStyled,
  FooterStyled,
  AddTodoList,
} from './components/HomeStyled';
import checkList from '../../common/image/list.png';
import {
  createTodo,
  deleteTodo,
  updateTodo,
} from '../../reducers/store/TodoListActions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../reducers/store';
import { Todo } from '../../reducers/types';

const Home: FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const { TextArea } = Input;
  const [visibleDrawer, setVisibleDrawer] = useState<boolean>(false);
  const [note, setNote] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [editList, setEditList] = useState<Todo>();
  const data = useSelector((state: RootState) => state.todo.data);

  const showDrawer = (): void => {
    setVisibleDrawer(true);
  };

  const onClose = (): void => {
    setNote('');
    setDescription('');
    setVisibleDrawer(false);
  };

  const onAddNote = (): void => {
    dispatch(
      createTodo({
        id: uuid.v4(),
        note,
        description,
        checked: false,
      }),
    );
    setNote('');
    setDescription('');
    setVisibleDrawer(false);
  };

  const onCheckList = (todo: Todo): void => {
    dispatch(
      updateTodo({
        id: todo.id,
        note: todo.note,
        description: todo.description,
        checked: !todo.checked,
      }),
    );
  };

  const onEditList = (): void => {
    dispatch(
      updateTodo({
        id: editList?.id ? editList.id : '',
        note: note,
        description: description,
        checked: editList?.checked ? editList.checked : false,
      }),
    );
    setNote('');
    setDescription('');
    setVisibleDrawer(false);
    setEditList(undefined);
  };

  const onDeleteList = (todo: Todo): void => {
    dispatch(deleteTodo(todo));
  };

  return (
    <Container>
      <HeaderStyled>
        <div></div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <img src={checkList} width="56" height="64" />
          <span
            style={{
              fontSize: 32,
              fontWeight: 'normal',
              color: '#FFFFFF',
              marginLeft: 8,
            }}
          >
            TODO LIST
          </span>
        </div>
        <Button
          type="text"
          danger
          icon={<PoweroffOutlined />}
          onClick={() => {
            localStorage.removeItem('token');
            history.push('/login');
          }}
        />
      </HeaderStyled>
      <BodyStyled>
        {data.length > 0 &&
          data.map((todo: Todo) => (
            <ListStyled key={todo.id}>
              <div style={{ display: 'flex', flex: 1, alignItems: 'center' }}>
                <CheckBoxStyled onClick={() => onCheckList(todo)}>
                  {todo.checked ? (
                    <CheckOutlined
                      style={{
                        fontSize: '16px',
                        color: '#82f',
                      }}
                    />
                  ) : null}
                </CheckBoxStyled>
                <span
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: '#63f',
                    marginLeft: 8,
                  }}
                >
                  {todo.note} : {todo.description}
                </span>
              </div>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <CheckBoxStyled
                  onClick={() => {
                    setEditList(todo);
                    setNote(todo.note);
                    setDescription(todo.description);
                    setVisibleDrawer(true);
                  }}
                >
                  <EditOutlined
                    style={{
                      fontSize: '16px',
                      color: '#82f',
                    }}
                  />
                </CheckBoxStyled>
                <CheckBoxStyled onClick={() => onDeleteList(todo)}>
                  <DeleteOutlined
                    style={{
                      fontSize: '16px',
                      color: '#82f',
                    }}
                  />
                </CheckBoxStyled>
              </div>
            </ListStyled>
          ))}
      </BodyStyled>
      <FooterStyled>
        <CircleButtonStyled onClick={() => showDrawer()}>
          <PlusOutlined
            style={{
              fontSize: '16px',
              color: '#ffffff',
            }}
          />
        </CircleButtonStyled>
      </FooterStyled>
      <Drawer
        width={420}
        title="Add item..."
        placement="right"
        closable={false}
        onClose={onClose}
        visible={visibleDrawer}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                label="Note"
                rules={[{ required: true, message: 'Please enter user note' }]}
              >
                <Input
                  placeholder="Please enter user note"
                  value={note}
                  onChange={(e) => {
                    setNote(e.target.value);
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                label="Description"
                rules={[
                  {
                    required: true,
                    message: 'please enter url description',
                  },
                ]}
              >
                <TextArea
                  autoSize={{ minRows: 3, maxRows: 5 }}
                  value={description}
                  onChange={(e) => {
                    setDescription(e.target.value);
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <AddTodoList
          onClick={() => {
            editList ? onEditList() : onAddNote();
          }}
        >
          <span style={{ fontSize: 18, fontWeight: 'normal', color: '#fff' }}>
            Add
          </span>
        </AddTodoList>
      </Drawer>
    </Container>
  );
};

export default Home;
