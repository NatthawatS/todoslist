import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex: 1;
  height: 100vh;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  padding: 16px;
  background-color: #020631;
`;
export const HeaderStyled = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 32px;
`;

export const BodyStyled = styled.div`
  display: flex;
  flex: 8;
  flex-direction: column;
  width: 100%;
  overflow-y: scroll;
`;

export const FooterStyled = styled.div`
  display: flex;
  flex: 1;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const ListStyled = styled.div`
  background-color: #ffffff;
  display: flex;
  align-items: center;
  flex-direction: row;
  border-radius: 6px;
  padding: 16px;
  justify-content: space-between;
  margin-bottom: 16px;
  box-shadow: rgba(33, 35, 38, 0.2) 0px 7px 29px 0px;
`;

export const CheckBoxStyled = styled.div`
  width: 36px;
  height: 36px;
  border-radius: 6px;
  border-width: 1px;
  background-color: #ffffff;
  justify-content: center;
  align-items: center;
  display: flex;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
  margin: 0 4px 0 4px;
`;

export const CircleButtonStyled = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 16px;
  background: -webkit-linear-gradient(
    -45deg,
    #e21df4 0%,
    #7513f4 51%,
    #3d0da5 94%
  );
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
`;

export const AddTodoList = styled.div`
  display: flex;
  position: absolute;
  bottom: 20px;
  width: 120px;
  height: 40px;
  align-items: center;
  justify-content: center;
  border-radius: 25px;
  background: -webkit-linear-gradient(
    -45deg,
    #e21df4 0%,
    #7513f4 51%,
    #3d0da5 94%
  );
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
`;
