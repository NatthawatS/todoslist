import React, { FC, useState } from 'react';
import { Form, Input, Button, Card, Typography, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Container } from '../../components/Container';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { loginFailure, loginSuccess } from '../../reducers/store/LoginActions';
import { baseUrl } from '../../api/config';

interface onSubmitTypes {
  username: string;
  password: string;
}

const Login: FC = () => {
  const { Title } = Typography;
  const [submitting, setSubmitting] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();

  const onFinish = async (values: onSubmitTypes) => {
    setSubmitting(true);
    const request = {
      username: values.username,
      password: values.password,
    };

    const response = await axios.post(baseUrl + '/users/auth', request);
    const { status } = response;
    switch (status) {
      case 200:
        localStorage.setItem('token', response.data.token);
        dispatch(loginSuccess());
        setSubmitting(false);
        history.push('/');
        break;
      default:
        dispatch(loginFailure(response.data.message || ''));
        notification.error({
          message: 'Error',
          description: `${response.data.message}`,
        });
        setSubmitting(false);
        break;
    }
  };

  return (
    <Container>
      <Card
        style={{
          padding: 16,
          width: 400,
        }}
      >
        <Title
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          level={3}
        >
          <UserOutlined /> Login
        </Title>

        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: 'Please input your Username!' }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: 'Please input your Password!' }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Button
            loading={submitting}
            type="primary"
            htmlType="submit"
            className="login-form-button"
            style={{
              flex: 1,
              display: 'flex',
              width: '100%',
              justifyContent: 'center',
            }}
          >
            Log in
          </Button>
        </Form>
      </Card>
    </Container>
  );
};

export default Login;
