import React from 'react';
import {
  withRouter,
  Switch,
  Route,
  Redirect,
  BrowserRouter as Router,
} from 'react-router-dom';
import { Layout } from 'antd';
import Home from '../pages/Home';
import Login from '../pages/Login';
import ProtectedRoute from './ProtectedRoute';

const Routes = () => (
  <Router>
    <Layout style={{ minHeight: '100vh' }}>
      <Switch>
        <Route exact path="/login" component={Login} />
        <ProtectedRoute exact path="/" component={Home} />
      </Switch>
    </Layout>
  </Router>
);

export default Routes;
