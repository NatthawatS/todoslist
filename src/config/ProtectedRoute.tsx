import React, { FunctionComponent } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../reducers/store';

interface PrivateRouteProps extends RouteProps {
  component: FunctionComponent;
}
const PrivateRoute = (props: PrivateRouteProps) => {
  const { component: Component, ...rest } = props;
  const auth = useSelector((state: RootState) => state.auth.isAuthenticated);

  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
