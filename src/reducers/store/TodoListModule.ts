import { TodoState, TodoActionsTypes, TodoRequestType } from '../types';

const initialState: TodoState = {
  data: [],
};

export default function todoReducer(
  state = initialState,
  action: TodoActionsTypes,
): TodoState {
  switch (action.type) {
    case TodoRequestType.CreateTodoRequest:
      return {
        data: [...state.data, action.payload.todo],
      };
    case TodoRequestType.UpdateTodoRequest: {
      const data = state.data.map((todo) =>
        todo.id === action.payload.todo.id ? action.payload.todo : todo,
      );
      return { data };
    }
    case TodoRequestType.DeleteTodoRequest: {
      const data = state.data.filter(
        (todo) => todo.id !== action.payload.todo.id,
      );
      return { data };
    }
    default:
      return state;
  }
}
