import { TodoActionsTypes, Todo, TodoRequestType } from '../types';

export function createTodo(todo: Todo): TodoActionsTypes {
  return {
    type: TodoRequestType.CreateTodoRequest,
    payload: { todo },
  };
}

export function updateTodo(todo: Todo): TodoActionsTypes {
  return {
    type: TodoRequestType.UpdateTodoRequest,
    payload: { todo },
  };
}

export function deleteTodo(todo: Todo): TodoActionsTypes {
  return {
    type: TodoRequestType.DeleteTodoRequest,
    payload: { todo },
  };
}
