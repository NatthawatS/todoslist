import { combineReducers } from 'redux';
import authReducer from './LoginModule';
import TodoListModule from './TodoListModule';

const AppReducer = combineReducers({
  todo: TodoListModule,
  auth: authReducer,
});

export type RootState = ReturnType<typeof AppReducer>;
export default AppReducer;
