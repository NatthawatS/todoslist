import { LoginRequestType } from '../types';

export function loginSuccess() {
  return {
    type: LoginRequestType.LoginSuccessRequest,
  };
}

export function loginFailure(message: string) {
  return {
    type: LoginRequestType.LoginFailureRequest,
    message: message,
  };
}
