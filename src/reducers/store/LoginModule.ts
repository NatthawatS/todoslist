import { LoginActionsTypes, LoginRequestType, LoginState } from '../types';

const initialState: LoginState = {
  isAuthenticated: localStorage.getItem('token') !== null,
};

export default function authReducer(
  state: LoginState = initialState,
  action: LoginActionsTypes,
) {
  switch (action.type) {
    case LoginRequestType.LoginSuccessRequest:
      return Object.assign({}, state, {
        isAuthenticated: true,
      });
    case LoginRequestType.LoginFailureRequest:
      return state;
    default:
      return state;
  }
}
