import { createStore } from 'redux';
import reducer from './store';

export default createStore(reducer);
