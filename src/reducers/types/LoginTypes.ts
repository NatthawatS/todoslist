export interface LoginState {
  isAuthenticated: boolean;
}

export enum LoginRequestType {
  LoginSuccessRequest = 'LoginRequestType/LoginSuccessRequest',
  LoginFailureRequest = 'LoginRequestType/LoginFailure_Request',
}

interface LoginSuccessRequest {
  type: typeof LoginRequestType.LoginSuccessRequest;
  payload: { LoginState: LoginState };
}

interface LoginFailureRequest {
  type: typeof LoginRequestType.LoginFailureRequest;
  payload: { LoginState: LoginState };
}

export type LoginActionsTypes = LoginSuccessRequest | LoginFailureRequest;
