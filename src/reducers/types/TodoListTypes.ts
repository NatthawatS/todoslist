export interface Todo {
  id: string;
  note: string;
  description: string;
  checked: boolean;
}

export interface TodoState {
  data: Todo[];
}

export enum TodoRequestType {
  CreateTodoRequest = 'TodoRequestType/CreateTodoRequest',
  UpdateTodoRequest = 'TodoRequestType/UpdateTodoRequest',
  DeleteTodoRequest = 'TodoRequestType/DeleteTodoRequest',
}

interface CreateTodoRequest {
  type: typeof TodoRequestType.CreateTodoRequest;
  payload: { todo: Todo };
}

interface UpdateTodoRequest {
  type: typeof TodoRequestType.UpdateTodoRequest;
  payload: { todo: Todo };
}

interface DeleteTodoRequest {
  type: typeof TodoRequestType.DeleteTodoRequest;
  payload: { todo: Todo };
}

export type TodoActionsTypes =
  | CreateTodoRequest
  | UpdateTodoRequest
  | DeleteTodoRequest;
